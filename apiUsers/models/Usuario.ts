export class Usuario{
    
    public id: string;
    public correo: string;
    public fotoURL: string;
    public nombreCompleto: string;
    public status:string;
    public room:string;

    constructor(id:string,correo:string = 'TBD',fotoURL:string = '',nombreCompleto:string='',status:string='',room:string=''){
        this.id=id;
        this.correo = correo;
        this.fotoURL = fotoURL;
        this.nombreCompleto = nombreCompleto;
        this.status = status;
        this.room = room;
    }
    
}