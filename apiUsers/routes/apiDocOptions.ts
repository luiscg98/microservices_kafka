export const options ={
    definition: {
        openapi: "3.0.0",
        info: {
            title: "API Backend",
            version: "1.0.0",
            description: "Documentación de API-Backend"
        },
        servers: [
            {
                url:"apibackend.manenova.me"
            }
        ],
      
    },
    apis: ["./routes/*.ts"]
}