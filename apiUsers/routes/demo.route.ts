import {Router} from 'express';
import DemoController from '../controllers/demo.controller';
import Token from '../middlewares/token.middleware';

const router: Router = Router();
const demoController = DemoController();
const token = Token();

router.get('/', demoController.testing);
router.post('/createToken', demoController.createToken);
router.post('/endPoint', token.verify, demoController.endPoint);

export default router;