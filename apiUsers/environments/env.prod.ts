export default {
    API:{
        ENVIRONMENT: process.env.NODE_ENV,
        NAME: `API Real-Time ${process.env.NODE_ENV}`,
        PORT: process.env.API_PORT
    },
    MONGODB: {
        ENVIRONMENT: process.env.NODE_ENV,
        HOST:'localhost',
        PORT: process.env.MONGODB_PORT,
        USER_NAME: process.env.MONGODB_USER_NAME,
        USER_PASSWORD: process.env.MONGODB_USER_PASSWORD,
        DATABASE: process.env.MONGODB_DATABASE
    },
    TOKEN: {
        EXPIRES: 60*60*12,
    },
    GMAIL:{
        user: process.env.GMAIL_USER,
        pass: process.env.GMAIL_PASSWORD
    },
    SERVER:{
        path:"http://appseguros.manenova.me/#/",
        verification:"account-verification/",
        dominio:"http://localhost:4200",
        //dominio:"http://appseguros.manenova.me",
    },
    SERVER_KAFKA:{
        HOST:'kafkaseguros.manenova.me',
        PORT:'9092'
    }
}