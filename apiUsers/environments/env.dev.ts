export default {
    API:{
        ENVIRONMENT: process.env.NODE_ENV,
        NAME: `API Real-Time ${process.env.NODE_ENV}`,
        PORT: 3000
    },
    MONGODB: {
        ENVIRONMENT: process.env.NODE_ENV,
        HOST: 'localhost',
        PORT: 27017,
        USER_NAME: '',
        USER_PASSWORD: '',
        DATABASE: 'usuariosRta',
    },
    TOKEN: {
        EXPIRES: 60*60*12,
    },
    SERVER_KAFKA:{
        HOST:'192.168.1.162',
        PORT:'9092'
    }
}