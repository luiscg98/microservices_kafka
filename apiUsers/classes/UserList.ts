import { Usuario } from '../models/Usuario';

export class UserList {

    private _mongo: any;

    constructor(mongo: any) {
        this._mongo = mongo;
    }

    public async changeSocket(socketId:string,usuario:string){
        await this._mongo.db.collection('sockets').updateOne(
            {socketId},
            {
                $set:{
                    usuario
                }
            }
        )
    }

    public async deleteSocket(id: string) {
        await this._mongo.db.collection('sockets').remove({ socketId: id });
    }

    public async getOnlineSockets() {

        const listUser: Usuario[] = [];

        const response = await this._mongo.db.collection('sockets')
            .aggregate([
                {
                    "$match": { "usuario": { $ne: null } }
                },
                {
                    $lookup:
                    {
                        from: 'usuarios',
                        localField: 'usuario',
                        foreignField: 'correo',
                        as: 'Usuario',
                    }
                }
            ])
            .toArray();

        for (var i in response) {
            listUser.push(
                new Usuario(
                    response[i].socketId,
                    response[i].Usuario[0].correo,
                    response[i].Usuario[0].fotoURL,
                    response[i].Usuario[0].nombreCompleto,
                    response[i].estatus,
                    ''
                )
            )
        }

        return listUser
    }

    public async getOnlineSocketsByFilter(filter:any){
       
    }


}