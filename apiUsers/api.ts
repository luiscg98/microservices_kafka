// Settings Import
import Settings from './settings';
// Import Color Util
import ColorUtilLib from './utils/color.util';
// Import Debug Util
import DebugUtilLib from './utils/debug.util';
// Import Mongo Helper
import MongoHelper from './helpers/mongo.helper';
import KafkaHelper from './helpers/kafka.helper';
// Import Express
import Express from 'express';
// Import Express Rate Limit
import rateLimit from 'express-rate-limit';
//Import Express Fileupload
import fileUpload from 'express-fileupload';
// Import CORS
import cors from 'cors';
// Import Morgan
import morgan from 'morgan';
//Import Helmet Hardenning Express;
import helmet from 'helmet';
//Import Compression
import compression from 'compression';
//Import Swagger
import swaggerUI from 'swagger-ui-express';
import swaggerJSDoc from 'swagger-jsdoc';
import {options} from './routes/apiDocOptions';
//import {options} from './routes/apiDocOptions';
//Import Express API Server
import ExpressAPIServer from './servers/web.server';
import { Socket } from 'socket.io';

import SocketLogic from './sockets/socket.logic';
import SocketSegurosLogic from './sockets/socket-seguros.logic';
import TokenHelper from './helpers/token.helper';
// Import Routes

import DemoRoute from './routes/demo.route';


import kafka, { Consumer } from 'kafka-node';

const dotenv = require('dotenv');

// Constants Declarations
const settings = Settings();
const color = ColorUtilLib();
const debug = DebugUtilLib();
const specs = swaggerJSDoc(options);


(async () => {

    dotenv.config();
    //Se obtiene las configuraciones de acuerdo al ambiente (DEV|QA|PROD)
    global.ENV = await settings.environment();
    const ENV: any = global.ENV;

    const mongo = MongoHelper.getInstance(ENV);
    const tokenHelper = TokenHelper(ENV, mongo);
    const kafkaHelper = KafkaHelper(global.ENV);

    // Connect to Kafka Server 
    const cnn = new kafka.KafkaClient({ kafkaHost: `${ENV.SERVER_KAFKA.HOST}:${ENV.SERVER_KAFKA.PORT}`, });

    // Conexión a la Base de Datos de MongoDB
    await mongo.connect(ENV.MONGODB.DATABASE);
    if (mongo.infoConnection.status == 'error') {
        debug.unhandleError(`${color.danger(mongo.infoConnection.msg)}`);
        // Cerrar Conexión de MongoDB
        await mongo.disconnect();
        debug.mongoDB(`${color.mongoDB('MongoDB:')} ${color.danger('Lo sentimos la conexión al servidor de MongoDB no se pudo realizar')}`);
        process.exit(0);
    }
    //Limpiar la tabla de Sockets
    await mongo.db.collection('sockets').deleteMany({});
    // Obtener WhiteList de Dominios Permitidos
    const config: any = await mongo.db.collection('c_config').find({}).toArray();
    /*******************
    * Start Express API Server *
    *******************/
    const www = ExpressAPIServer.getInstance(ENV.API.PORT, config[0].allowDomains);
    /**********************
    * Middlewares de la API
    ***********************/
    // Habilitar hardenning en Express Servers
    www.api.use(helmet());
    //Habilitar Encodig JSON
    www.api.use(Express.json());
    www.api.use(Express.urlencoded({ extended: true }));
    //Habilitar uso de fileupload
    www.api.use(fileUpload({ limits: { fileSize: 50 * 4096 * 4096 } }));

    www.api.use('/v1/doc/', swaggerUI.serve, swaggerUI.setup(specs));

    // Habilitar el Uso de CORS
    const corsOptionsDelegate = (req: any, callback: Function) => {
        let error: any = null;
        let corsOptions: any = {};
        let isDomainAllowed = config[0].allowDomains.indexOf(req.header('Origin')) !== -1;
        if (isDomainAllowed) {
            // Habilitar CORS a la Solicitud para la lista de Dominios Permitidos
            corsOptions = {
                origin: true,
                optionsSuccessStatus: 200
            }
        } else {
            // Denegar la Petición
            error = `La política de CORS ha bloqueado el acceso para la petición del origen ${req.header('Origin')} `
        }
        callback(error, corsOptions);
    };

    //www.api.use(cors(corsOptionsDelegate));
    www.api.use(cors({ origin: true, credentials: true }));
    //Habilitar Morgan
    www.api.use(morgan('dev'));
    //Protección contra ataques de denegación de servicio
    www.api.enable('trust proxy');
    const limiter = rateLimit({
        windowMs: 1 * 60 * 1000,
        max: 2000
    });
    www.api.use(limiter);
    //Habilitar Compression
    // www.api.use(compression({
    //     filter: (req: any, res: any) => compression.filter(req, res),
    //     level: 6
    // }));

    // Rutas de la API
    www.api.use('/api', DemoRoute);
    //www.api.use('/v1/email/', apiEmail);
    //www.api.use('/v1/message/', apiMessage);
    //www.api.use('/v1/services/', apiServices);
  
    // Termina Rutas de la API


    // Funcionalidad Real-Time
    const socketLogic = SocketLogic(mongo, ENV, tokenHelper);
    const socketSegurosLogic = SocketSegurosLogic(mongo, ENV, tokenHelper, kafkaHelper);

    www.io.on('connection', (socket: Socket) => {
        socketLogic.signUp(www.io, socket);
        socketLogic.logOut(www.io, socket);
        socketLogic.disconnect(www.io, socket);
        socketLogic.getUserOnline(www.io, socket);
        socketLogic.listenSocketConnect(socket);
        socketLogic.putOnlineSocket(www.io, socket);
        socketLogic.getUserOnlineBroadcast(www.io, socket);
        socketLogic.privateMessage(www.io, socket);
        //Sockets Proyecto
        socketSegurosLogic.getHospitales(www.io, socket);
        socketSegurosLogic.generateIncidente(www.io, socket);
        socketSegurosLogic.getCheckSeguro(www.io, socket);
        socketSegurosLogic.generateCitaHospital(www.io, socket);
        socketSegurosLogic.getHistorialMedico(www.io, socket);
    });

    //Kafka Consumidor
    const consumer: any = new kafka.Consumer(cnn, [{ topic: 'test' }], {});

    consumer.on('message', async (msg: any) => {

        const topic = msg.topic;
        //const valor: any = JSON.parse(msg.value);

        //const socket = await mongo.db.collection('sockets').find({"usuario":valor.correo}).toArray();
                
        switch (topic) {
            case 'test':
                // for (var i in socket) {
                //     const socketId = socket[i].socketId;
                //     www.io.to(socketId).emit('response_allredmedica_90f71ca63f07',valor);
                // }
               console.log(msg.value);
            break;
        }

    });

    www.start(() => {
        debug.express(`${color.express(`Express:`)} ${color.express(`Servidor funcionando correctamente en el puerto ${ENV.API.PORT}`)}`);
    });

})();


//Handle Error
process.on('unhandleRejection', (error: any, promise) => {
    console.log(`Ocurrió un error no controlado de tipo promise rejection`, promise);
    console.log(`La descripción de error es la siguiente `, error);
    //Close Mongo 
    process.exit();
});