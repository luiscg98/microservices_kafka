import { Socket } from "socket.io"
import { UserList } from '../classes/UserList';
import socketIO from 'socket.io';
export default (mongo: any, ENV: any, tokenHelper: any) => {

    const usersList = new UserList(mongo);

    return {

        listenSocketConnect: async (socket: Socket) => {
            await mongo.db.collection('sockets').insertOne({
                socketId: socket.id,
                usuario: null,
                estatus: 'pending'
            }).then((result: any) => console.log(result))
                .catch((error: any) => console.log(error));
        },
        putOnlineSocket: async (io: socketIO.Server, socket: Socket) => {

            socket.on('putOnline-user', async (payload: any) => {
                await mongo.db.collection('sockets').findOneAndUpdate(
                    { socketId: socket.id },
                    {
                        $set: {
                            usuario: payload.email,
                            estatus: payload.estatus,
                        }
                    },
                    {
                        upsert: true,
                    }
                )
                io.emit('users-online', await usersList.getOnlineSockets());
            });
        },
        signUp: (io: socketIO.Server, socket: Socket) => {

            var token_: string = '';
            var status_: boolean = false;
            var code_: number = 0;
            var msg_: string = '';

            //Metodo para registrar Usuario
            socket.on('signUp', async (payload: any) => {
                /*
                //Guardar en Base de Datos
                await mongo.db.collection('usuarios').findOneAndUpdate(
                    { correo: payload.email },//Criterio de Busqueda   
                    {
                        $setOnInsert: {
                            isVerify: false
                        },
                        $set: {
                            nombreCompleto: payload.fullName,
                            fotoURL: payload.photoUrl
                        }
                    },
                    {
                        upsert: true
                    }
                ).then(async (result: any) => {
                      
                        if (!result.lastErrorObject.updatedExisting) {
                           
                            await mongo.db.collection('usuarios')
                            .findOne(
                                { correo:payload.email, isVerify: false },
                                { projection: {_id:0, correo:1,fotoURL:1, nombreCompleto:1}}
                            )
                            .then(async (result_user: any) => {
                                if (result_user) {
                                    status_=true;
                                    code_=200;
                                    msg_=`Excelente, se registro correctamente el correo electronico ${payload.email}`;

                                    const token: any = await tokenHelper.create(result_user, payload.apiKey);
                                   
                                    await transporter.sendMail({
                                        from: '"Chat MTWDyM  - Please confirm your email address." <consnova@gmail.com>', // sender address
                                        to: payload.email, // list of receivers
                                        subject: "Chat MTWDyM - Please confirm your email address.", // Subject line
                                        html: `
                                            <p>Thanks for signing up for Chat MTWDyM ! Please click the link below to confirm your email address.</p>
                                            <a href="${ENV.SERVER.path}${ENV.SERVER.verification}${token.token}">${ENV.SERVER.path}${ENV.SERVER.verification}${token.token}</a>
                                            <p>Happy coding!</p>
                                            <p>Team Chat MTWDyM </p>
                                        `
                                    });
                                    
                                    await mongo.db.collection('verification.emails').findOneAndUpdate(
                                        { correo: payload.email },//Criterio de Busqueda   
                                        {
                                            $set: {
                                                tokenConfirmation: token.token
                                            }
                                        },
                                        {
                                            upsert: true
                                        }
                                    );
                                    
                                    const hashDigest = sha256(token);
                                    token_ = Base64.stringify(hmacSHA512(hashDigest,payload.apiKey));

                                    await mongo.db.collection('confirmation.emails').findOneAndUpdate(
                                        { correo: payload.email },//Criterio de Busqueda   
                                        {
                                            $set: {
                                                tokenConfirmation: token_
                                            }
                                        },
                                        {
                                            upsert: true
                                        }
                                    );
                                }else{
                                    token_ = '';
                                    msg_=`Error, no fue posible registrar el correo electronico ${payload.email}.`;
                                    code_ = 404;
                                    status_=false;
                                }
                            }).catch((error:any)=>{
                                token_ = '';
                                msg_=error;
                                code_=500;
                                status_=false;
                            });

                        } else {
                            token_ = '';
                            msg_=`El correo electronico ${payload.email} no fue posible registrar ya que este se encuentra registrado en el sistema`;
                            code_ = 200;
                            status_=false;
                        }

                        io.to(socket.id).emit('register-status', {
                            ok: status_,
                            token: token_,
                            msg: msg_,
                            code: code_
                        });

                    })
                    .catch((error: any) => console.log(error));*/

            });
        },
        disconnect: (io: socketIO.Server, socket: Socket) => {
            socket.on('disconnect', async () => {
                //Cambiado por B.D.
                await usersList.deleteSocket(socket.id);
                io.emit('users-online', await usersList.getOnlineSockets());
            });
        },
        logOut: (io: socketIO.Server, socket: Socket) => {
            socket.on('logOut', async () => {
                //Cambiado por B.D.
                console.log("HOLA MANE ESTOY EJECUTANDO EL LOGOUT REVISA=======");
                await usersList.deleteSocket(socket.id);
                io.emit('users-online', await usersList.getOnlineSockets());
            });
        },
        getUserOnline: (io: socketIO.Server, socket: Socket) => {
            socket.on('get-users-online', async () => {
                io.to(socket.id).emit('users-online', await usersList.getOnlineSockets());
            });
        },
        getUserOnlineBroadcast: (io: socketIO.Server, socket: Socket) => {
            socket.on('get-users-online-broadcast', async () => {
                io.emit('users-online', await usersList.getOnlineSockets());
            });
        },
        privateMessage: (io: socketIO.Server, socket: Socket) => {

            socket.on('replies-private-message', async (payload: any) => {

                await mongo.db.collection('messages').insertOne({
                    type: payload.type,
                    message: payload.message,
                    from: payload.from,
                    to: payload.to,
                    room: payload.room,
                    view: payload.view,
                    dateSend: new Date(),
                    fromSocket: payload.fromSocket,
                    toSocket: payload.toSocket,
                    fromPhoto: payload.fromPhoto
                }).then(async (result: any) => {
                    const sockets = await mongo.db.collection('sockets').find({ usuario: payload.to }).toArray();
                    for (var i in sockets) {
                        payload.toSocket = sockets[i].socketId;
                        io.to(payload.toSocket).emit('send-private-message', payload);
                    }

                }).catch((err: any) => {
                    console.log(err);
                });

            });
        }

    }
}