import { Socket } from "socket.io";
import socketIO from 'socket.io';

export default (mongo: any, ENV: any, tokenHelper: any, KafkaHelper: any) => {

    return {
        getHospitales: async (io: socketIO.Server, socket: Socket) => {

            socket.on('get_redmedica-90f71ca63f07', async (payload: any) => {

                const { token, apiKey, typePlan, Lat, Lng } = payload;
                const result: any = await tokenHelper.verify(token, apiKey);
            
                const customer: any = {
                    id: result.tokenDecoded._id,
                    correo: result.tokenDecoded.correo,
                    Lat,
                    Lng,
                    typePlan
                }
                KafkaHelper.getAllRedMedica(JSON.stringify(customer));

            });
        },

        generateIncidente:async (io: socketIO.Server, socket: Socket) =>{
            socket.on('generate_incidenteseguros-688e104f51e4', async (payload: any) => {
               
                const { token, apiKey, id_hospital, name_hospital, id_poliza, typePlan, typeIncident,Lat, Lng } = payload;
                const result: any = await tokenHelper.verify(token, apiKey);
                
                const customer: any = {
                    id: result.tokenDecoded._id,
                    correo: result.tokenDecoded.correo,
                    id_hospital,
                    name_hospital,
                    typePlan,
                    id_poliza,
                    typeIncident,
                    Lat,
                    Lng, 
                }
                KafkaHelper.generateIncidente(JSON.stringify(customer));
            });
        },

        getCheckSeguro:async (io: socketIO.Server, socket: Socket) =>{

            socket.on('check_seguro-d18b946e790f', async (payload: any) => {
               
                const { token, apiKey} = payload;
                const result: any = await tokenHelper.verify(token, apiKey);
                
                const customer: any = {
                    _id: result.tokenDecoded._id,
                    correo: result.tokenDecoded.correo,
                }

                KafkaHelper.checkSeguro(JSON.stringify(customer));
            });

           
        },
        generateCitaHospital:async(io: socketIO.Server, socket: Socket)=>{
            socket.on('generate_citamedica-688e104f51e4', async (payload: any) => {
               
                const { token, apiKey, id_hospital, nombreHostipal, id_poliza, typePlan, location,typeIncident,numeIncidente} = payload;
                const result: any = await tokenHelper.verify(token, apiKey);
                
                const customer: any = {
                    id_usuario: result.tokenDecoded._id,
                    id_hospital,
                    id_poliza,
                    typePlan,
                    location,
                    nombreHostipal,
                    correo: result.tokenDecoded.correo,
                    typeIncident,
                    numeIncidente
                }
                KafkaHelper.generateCitaHospital(JSON.stringify(customer));
            });

        },
        getHistorialMedico : async (io: socketIO.Server, socket: Socket) => {
            socket.on('get_historialmedicoe0025fc56a08', async (payload: any) => {
               
                const { token, apiKey } = payload;
                const result: any = await tokenHelper.verify(token, apiKey);
                
                const customer: any = {
                    _id: result.tokenDecoded._id,
                    correo: result.tokenDecoded.correo,
                }
                KafkaHelper.getHistorialMedico(JSON.stringify(customer));
            });
        }
    }
}