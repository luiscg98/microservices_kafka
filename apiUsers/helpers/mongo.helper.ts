import { MongoClient, MongoClientOptions } from 'mongodb';
// Debug Util Lib Import
import DebugUtilLib from '../utils/debug.util';
// Color Util Lib Import
import ColorUtilLib from '../utils/color.util';

import { debug } from 'console';

export default class MongoDBHelper {

    public db: any;
    public infoConnection: any = {};

    private static _instance: MongoDBHelper;
    private cnn: any;
    private dbUri: string;
    private ENV: any;
    
    constructor(env: any) {
        this.ENV = env;
        if(this.ENV.API.ENVIRONMENT === 'DEV ')
        {
            this.dbUri = `mongodb://${this.ENV.MONGODB.HOST}:${this.ENV.MONGODB.PORT}/${this.ENV.MONGODB.DATABASE}`;
        }
        else{
            this.dbUri = `mongodb://${this.ENV.MONGODB.USER_NAME}:${this.ENV.MONGODB.USER_PASSWORD}@${this.ENV.MONGODB.HOST}:${this.ENV.MONGODB.PORT}/${this.ENV.MONGODB.DATABASE}?authSource=admin`;
        }
    };

    public static getInstance(env: any) {
        return this._instance || (this._instance = new this(env));
    }
    
    public async connect(dataBase: string, options: MongoClientOptions = { useNewUrlParser: true, useUnifiedTopology: true }) {

        const result = await MongoClient.connect(this.dbUri, options)
            .then((cnn: any) => {
                return { status: 'success', cnn, err: null, msg: `Servidor MongoDB corriendo exitosamente en el puerto ${this.ENV.MONGODB.PORT}` };
            })
            .catch((err: any) => {
                return { status: 'error', cnn: null, err, msg: `Ocurrio un Error al intentar establecer conexión con el Servidor de MongoDB en el puerto ${this.ENV.MONGODB.PORT}` };
            });
        
        this.infoConnection = result;
        if(this.infoConnection.status === 'success') {
            DebugUtilLib().mongoDB(`${ColorUtilLib().mongoDB('MongoDB:')} ${ColorUtilLib().info('La conexión al servidor se reaizó')} ${ColorUtilLib().success('correctamente')} ${ColorUtilLib().info('en el puerto')} ${ColorUtilLib().mongoDB(this.ENV.MONGODB.PORT)}`);
            this.cnn = this.infoConnection.cnn;
            this.db = this.cnn.db(dataBase);
        } else {
            this.cnn = null;
            this.db = null;
        }

    }
    public setDataBase(dataBase: string) {
        this.db = this.cnn.db(dataBase);
    }
    public async disconnect() {
        if (this.cnn != null) {
            this.cnn.close();
        }
    }

}