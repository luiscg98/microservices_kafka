import jwt from 'jsonwebtoken';

export default (ENV: any, mongo: any) => {

    const _getSecretKey = async (apiKey: string) => {
        //Utilizar la API KEY para consulta la tabla app y obtener el SecretKey
        return await mongo.db.collection("c_config").findOne(
            { apiKey },
            { project: { id: 0, apiSecret: 1 } }
        ).then((result: string) => {
            if (!result) {
                return null;
            }
            return result;
        })
            .catch((error: any) => {
                console.log(error);
                return null;
            });
    }

    return {
        create: async (payload: any, apiKey: string) => {

            const response: any = await _getSecretKey(apiKey);

            return new Promise((resolve: any, reject: any) => {
                jwt.sign(payload, response.apiSecret, { expiresIn: ENV.TOKEN.EXPIRES }, (error: any, token: any) => {
                    if (error) {
                        return reject({ ok: false, error });
                    }
                    return resolve({ ok: true, token });
                });
            });
        },
        verify: async (bearerToken: string, apiKey: string) => {

            const response: any = await _getSecretKey(apiKey);

            return jwt.verify(bearerToken, response.apiSecret, (error: any, tokenDecoded: any) => {
                if (error) {
                    return { ok: false, error };
                }
                return { ok: true, tokenDecoded };
            });



        },
        refresh: () => {
            // TO DO:
        }
    }
}