import kafka, { Consumer } from 'kafka-node';

export default (ENV: any) => {

    const cnn = new kafka.KafkaClient({
        kafkaHost: `${ENV.SERVER_KAFKA.HOST}:${ENV.SERVER_KAFKA.PORT}`,
    });

    const producer = new kafka.Producer(cnn);

    return {
        checkSeguro:(msg:any)=>{
            producer.send([{topic:'check_seguro-d18b946e790f', messages:msg}], (err:any, data:any)=>{ } );
        },

        getAllRedMedica:(msg:any)=>{
            producer.send([{topic:'get_allredmedica-90f71ca63f07', messages:msg}], (err:any, data:any)=>{ } );
        },

        generateIncidente:(msg:any)=>{
            producer.send([{topic:'generate_incidenteseguros-688e104f51e4', messages:msg}], (err:any, data:any)=>{ } );
        },

        generateCitaHospital:(msg:any)=>{
            producer.send([{topic:'generate_citamedica-688e104f51e4', messages:msg}], (err:any, data:any)=>{ } );
           
        },
        getHistorialMedico:(msg:any)=>{
            producer.send([{topic:'get_historialmedicoe0025fc56a08', messages:msg}], (err:any, data:any)=>{ } );
           
        }
        
        
    }

}