export default () => {
    return {
        environment: async () => {
            let env: any = {};
            switch(process.env.NODE_ENV){
                case 'PROD': env = await import('./environments/env.prod');
                    break;
                case 'QA': env = await import('./environments/env.qa');
                    break;
                case 'DEV': env = await import('./environments/env.dev');
                    break;
            }

            return env.default;
        }
    }
}