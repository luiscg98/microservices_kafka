import http from 'http';
import express from 'express';
import socket, {Server} from 'socket.io';
import Debug from '../utils/debug.util';
import Color from '../utils/color.util';

const debug = Debug();
const color = Color();
export default class ExpressAPIServer{
    
    //Patron singleton
    private static _instance: ExpressAPIServer;
    // Express API Server
    public httpServer: http.Server;
    public api: express.Application;
    public port :number;
    //Socket Server
    public io: Server;

    private constructor(port:number,whiteList:string[]){

        this.api = express();
        this.port = port;
        this.httpServer = http.createServer(this.api);
        this.io = new socket.Server(this.httpServer,{
            cors:{
                origin:whiteList,
                credentials:true
            },
            allowEIO3: true
           
        });
    }

    public static getInstance(port:number,whiteList:string[]){
        return this._instance || (this._instance = new this(port, whiteList));
    }

    start(callback: Function){
        this.httpServer.listen(this.port, callback());
        debug.express(`${color.express(`Express:`)} ${color.express(`Servidor funcionando correctamente en el puerto ${this.port}`)}`);
    
    }
};