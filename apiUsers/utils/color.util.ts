import color from 'chalk';

export default () => {
    return {
        express: color.bgHex('#333333').whiteBright.bold,
        socketIO: color.bgHex('#15465B').hex('#00dfff').bold,
        mongoDB: color.bgHex('#412F20').hex('#589636').bold,
        nodeJS: color.bgHex('#73AB63').hex('#ffffff').bold,

        success: color.greenBright.bold,
        danger: color.redBright.bold,
        warning: color.yellowBright.bold,
        info: color.blueBright.bold
    }
};