#!/bin/bash

docker exec -it Kafka kafka-topics.sh --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic check_seguro-d18b946e790f
docker exec -it Kafka kafka-topics.sh --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic response_check_seguro-d18b946e790f
docker exec -it Kafka kafka-topics.sh --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic get_redmedica-90f71ca63f07
docker exec -it Kafka kafka-topics.sh --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic response__redmedica-90f71ca63f07 
docker exec -it Kafka kafka-topics.sh --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic get_historialmedicoe0025fc56a08
docker exec -it Kafka kafka-topics.sh --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic response_historialmedicoe0025fc56a08
docker exec -it Kafka kafka-topics.sh --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic get_allredmedica-90f71ca63f07
docker exec -it Kafka kafka-topics.sh --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic response_allredmedica-90f71ca63f07

docker exec -it MongoDBRD mongoimport -u "dbo-operator" -p "LH7iiUyW4qdVuhS" --db "dbMTWyDM" --collection "e_hospitales" --drop --file "/scripts/e_hospitales.json"