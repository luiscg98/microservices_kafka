#!/bin/bash

MONGODB1=MongoDBSE

mongo --host ${MONGODB1}:27017 -u ${MONGO_INITDB_ROOT_USERNAME} -p ${MONGO_INITDB_ROOT_PASSWORD} <<EOF
    var cfg = {
        "_id": "rs1",
        "version":1,
        "members": [
            {
                "_id": 0,
                "host": "${MONGODB1}:27017",
                "priority": 1
            }
        ]
    };
    rs.initiate(cfg);
    rs.status();
EOF

sleep 15s

mongo --host ${MONGODB1}:27017 -u ${MONGO_INITDB_ROOT_USERNAME} -p ${MONGO_INITDB_ROOT_PASSWORD} <<EOF
    use dbMTWyDM;
    db.createUser({
        user: "dbo-operator",
        pwd: "LH7iiUyW4qdVuhS",
        roles: [
            {
                role: "readWrite",
                db: "dbMTWyDM"
            }
        ]
    });
    db.createCollection('c_config');
    db.getCollection('c_config').insert({
        "_id" : ObjectId("60b29eade772b3c684baa5a6"),
        "apiKey" : "API-KEY-60b29eade772b3c684baa5a6",
        "apiSecret" : "SECRET-KEY-50d38187b414a5b3d4ff2b8c2040d09eb1dc3898bcf9da2aebe3d664b9f232a4",
        "allowDomains" : [ 
            "http://example.com", 
            "http://angular.manenova.me", 
            "http://localhost"
        ]
    });

     db.createCollection('e_indicador');
    
EOF