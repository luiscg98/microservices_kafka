#!/bin/bash
        echo '========================================================'
        echo '=== PASO 1: INSTALACIÓN DE PREREQUISITOS PARA DOCKER ==='
        echo '========================================================'
        sudo apt-get update
        sudo apt-get install \
        apt-transport-https \
        ca-certificates \
        curl \
        unzip \
        gnupg-agent \
        software-properties-common -y
        sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
        sudo apt-key fingerprint 0EBFCD88

        echo '=============================================================='
        echo '=== PASO 2: AGREGAR REPOSITORIO PARA LA INSTALACIÓN DOCKER ==='
        echo '=============================================================='
        sudo add-apt-repository \
        "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
        $(lsb_release -cs) \
        stable"
        sudo apt-get update

        echo '====================================='
        echo '=== PASO 3: INSTALACIÓN DE DOCKER ==='
        echo '====================================='
        sudo apt-get install docker-ce docker-ce-cli containerd.io -y
        # Iniciar Docker junto con el Arranque del Sistema Operativo
        sudo systemctl enable docker
        # Agregar Usuario Actual al Grupo de Docker
        sudo usermod -aG docker root
        # Agregar Usuario gitlab-runner al Grupo de Docker
        sudo usermod -aG docker gitlab-runner

        echo '============================================='
        echo '=== PASO 4: INSTALACIÓN DE DOCKER-COMPOSE ==='
        echo '============================================='
        sudo curl -L https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
        sudo chmod +x /usr/local/bin/docker-compose

        echo '==============================================================='
        echo '=== PASO 5: INICIAR DOCKER AL ARRANCAR EL SISTEMA OPERATIVO ==='
        echo '==============================================================='
        sudo systemctl enable docker

        echo '========================================================='
        echo '=== PASO 6: AGREGAR USUARIO ACTUAL AL GRUPO DE DOCKER ==='
        echo '========================================================='
        sudo usermod -aG docker root

        echo '========================================='
        echo '=== PASO 7: INSTALAR HERRAMIENTA CTOP ==='
        echo '========================================='
        echo "deb http://packages.azlux.fr/debian/ buster main" | sudo tee /etc/apt/sources.list.d/azlux.list
        wget -qO - https://azlux.fr/repo.gpg.key | sudo apt-key add -
        sudo apt update
        sudo apt install docker-ctop
        
        echo '========================================================='
        echo '=== PASO 8: INSTALAR GIT Y CLONADO DE REPOS DE GITHUB ==='
        echo '========================================================='
        

        sudo mkdir -p /var/mongodb/mtw/node01/data/db/keyfile/ 
        sudo mkdir -p /var/mongodb/mtw/node02/data/db/keyfile/ 
        sudo mkdir -p /var/mongodb/mtw/node03/data/db/keyfile/

        openssl rand -base64 756 > mongo-keyfile
        sudo chmod 400 mongo-keyfile
        sudo chown 999 mongo-keyfile

        echo export API_PORT=3000 | tee -a .bashrc
        echo export API_KEY=API-KEY-60b29eade772b3c684baa5a6 | tee -a .bashrc
        echo export API_SECRET=SECRET-KEY-50d38187b414a5b3d4ff2b8c2040d09eb1dc3898bcf9da2aebe3d664b9f232a4 | tee -a .bashrc
        echo export MONGODB_HOST=localhost | tee -a .bashrc
        echo export MONGODB_PORT=27017 | tee -a .bashrc
        echo export MONGODB_USER_NAME=dbo-operator | tee -a .bashrc
        echo export MONGODB_USER_PASSWORD=LH7iiUyW4qdVuhS | tee -a .bashrc
        echo export MONGODB_DATABASE=dbMTWyDM | tee -a .bashrc
        echo export MONGO_ROOT_DATABASE=admin | tee -a .bashrc
        echo export MONGO_ROOT_USERNAME=dba-root | tee -a .bashrc
        echo export MONGO_ROOT_PASSWORD=LH7iiUyW4qdVuhS | tee -a .bashrc
        echo export API_PORT_RM=5000 | tee -a .bashrc
        echo export MONGODB_HOST_RM=localhost | tee -a .bashrc
        echo export MONGODB_PORT_RM=27019 | tee -a .bashrc
        echo export MONGODB_USER_NAME_RM=dbo-operator | tee -a .bashrc
        echo export MONGODB_USER_PASSWORD_RM=LH7iiUyW4qdVuhS | tee -a .bashrc
        echo export MONGODB_DATABASE_RM=dbMTWyDM | tee -a .bashrc
        echo export MONGO_ROOT_DATABASE_RM=admin | tee -a .bashrc
        echo export MONGO_ROOT_USERNAME_RM=dba-root | tee -a .bashrc
        echo export MONGO_ROOT_PASSWORD_RM=LH7iiUyW4qdVuhS | tee -a .bashrc
        echo export AWS_ACCESS_KEY_ID=AKIAWQDVGBG5D2SRLJE5 | tee -a .bashrc
        echo export AWS_SECRET_ACCESS_KEY=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 | tee -a .bashrc
        echo export GMAIL_USER=consnova@gmail.com | tee -a .bashrc
        echo export GMAIL_PASSWORD=ubaxccckditrwtrm | tee -a .bashrc
        
        echo export API_PORT=3000 | tee -a .profile
        echo export API_KEY=API-KEY-60b29eade772b3c684baa5a6 | tee -a .profile
        echo export API_SECRET=SECRET-KEY-50d38187b414a5b3d4ff2b8c2040d09eb1dc3898bcf9da2aebe3d664b9f232a4 | tee -a .profile
        echo export MONGODB_HOST=localhost | tee -a .profile
        echo export MONGODB_PORT=27017 | tee -a .profile
        echo export MONGODB_USER_NAME=dbo-operator | tee -a .profile
        echo export MONGODB_USER_PASSWORD=LH7iiUyW4qdVuhS | tee -a .profile
        echo export MONGODB_DATABASE=dbMTWyDM | tee -a .profile
        echo export MONGO_ROOT_DATABASE=admin | tee -a .profile
        echo export MONGO_ROOT_USERNAME=dba-root | tee -a .profile
        echo export MONGO_ROOT_PASSWORD=LH7iiUyW4qdVuhS | tee -a .profile
        echo export API_PORT_RM=5000 | tee -a .profile
        echo export MONGODB_HOST_RM=localhost | tee -a .profile
        echo export MONGODB_PORT_RM=27019 | tee -a .profile
        echo export MONGODB_USER_NAME_RM=dbo-operator | tee -a .profile
        echo export MONGODB_USER_PASSWORD_RM=LH7iiUyW4qdVuhS | tee -a .profile
        echo export MONGODB_DATABASE_RM=dbMTWyDM | tee -a .profile
        echo export MONGO_ROOT_DATABASE_RM=admin | tee -a .profile
        echo export MONGO_ROOT_USERNAME_RM=dba-root | tee -a .profile
        echo export MONGO_ROOT_PASSWORD_RM=LH7iiUyW4qdVuhS | tee -a .profile
        echo export AWS_ACCESS_KEY_ID=AKIAWQDVGBG5D2SRLJE5 | tee -a .profile
        echo export AWS_SECRET_ACCESS_KEY=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 | tee -a .profile
        echo export GMAIL_USER=consnova@gmail.com | tee -a .profile
        echo export GMAIL_PASSWORD=ubaxccckditrwtrm | tee -a .profile

        source .profile
        source .bashrc

        cd  /home/gitlab-runner/src/proyecto-03mtw309

        sudo docker-compose up --build -d

